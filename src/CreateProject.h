/*
 * Copyright 2021  Isho Antar <IshoAntar@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CREATEPROJECT_H
#define CREATEPROJECT_H

#include <KIOWidgets/KAbstractFileItemActionPlugin>
#include <KIOCore/KFileItem>
#include <KIOCore/KFileItemListProperties>
#include <QtWidgets/QAction>

#include <vector>
#include <filesystem>

/**
 * Version control system
 */
enum class Vcs
{
	None,
	Git
};

class CreateProject : public KAbstractFileItemActionPlugin
{
	Q_OBJECT

public:
	CreateProject(QObject* parent = nullptr, const QVariantList& args = QVariantList());
	virtual ~CreateProject() = default;
	QList<QAction*> actions(const KFileItemListProperties& fileItemInfos, QWidget* parentWidget) override;

private:
	void EnumerateTemplates();

private:
	std::vector<std::filesystem::path> m_ProjectTemplates;
	std::filesystem::path m_TemplatesDir;
};

#endif // CREATEPROJECT_H
