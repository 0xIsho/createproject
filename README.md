> This will not be maintained anymore. I'll be converting it to a standalone application when i have some free time!

# Dolphin Service Menu Plugin

This project is a plugin for dolphin which provides service menus.

It looks for project templates in `~/Templates/Projects/` (case-sensetive).

## Demo

![Demo GIF](misc/demo.gif)

## Build

Clone the repository:

```sh
git clone https://github.com/IshoAntar/CreateProject.git
```

Build and install:

```sh
cd CreateProject
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build . -j$(nproc)
sudo cmake --install .
```

The plugin can be disabled in `Configure Dolphin -> Context Menu -> uncheck CreateProject`


