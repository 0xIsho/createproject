1.1.0 - 2021.5.25
- Add support for custom templates.
- Add option to commit files on repository initialization.
- Disable the project type since it's not implemented, yet!

---

1.0 - 2021.5.25
- Initial release.